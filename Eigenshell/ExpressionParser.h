#pragma once
#ifndef EXPRESSION_PARSER_H
#define EXPERSSION_PARSER_H

#include <iterator>
#include <stack>
#include <string>
#include <vector>

using namespace std;


class ExpressionParser
{
public:
	enum tokenType { NUMBER, VARIABLE, OPERATOR };
	struct token
	{
		string tok;
		tokenType type;
	};

	static vector<token> tokenizeString(const string);
};

#endif