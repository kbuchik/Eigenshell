#pragma once
#ifndef SHELL_CPP
#define SHELL_CPP

#include "Shell.h"

/// ShellObj subclass function definitions

Shell::ShellObj::ShellObj()
{
	type = VAR_ERROR;
	error = "Object initialized with default constructor";
}

Shell::ShellObj::ShellObj(const ShellObj& obj)
{
	type = obj.type;
	error = obj.error;
	switch (type)
	{
	case VAR_INT:
		int_val = obj.int_val;
		break;
	case VAR_DOUBLE:
		dbl_val = obj.dbl_val;
		break;
	case VAR_MATRIX:
		mat_val = obj.mat_val;
		break;
	}
}

Shell::ShellObj::ShellObj(int data)
{
	type = VAR_INT;
	int_val = data;
	error = "";
}

Shell::ShellObj::ShellObj(double data)
{
	type = VAR_DOUBLE;
	dbl_val = data;
	error = "";
}

Shell::ShellObj::ShellObj(Matrix<double>& data)
{
	type = VAR_MATRIX;
	mat_val = data;
	error = "";
}

Shell::ShellObj::ShellObj(string errmsg)
{
	type = VAR_ERROR;
	error = errmsg;
}

Shell::ShellObj::~ShellObj()
{
	if (type == VAR_MATRIX)
		~mat_val;
}

Shell::ShellObj& Shell::ShellObj::operator=(const ShellObj& obj)
{
	type = obj.type;
	error = obj.error;
	switch (type)
	{
	case VAR_INT:
		int_val = obj.int_val;
		break;
	case VAR_DOUBLE:
		dbl_val = obj.dbl_val;
		break;
	case VAR_MATRIX:
		mat_val = obj.mat_val;
		break;
	}
	return *this;
}

string Shell::ShellObj::getError() const
{
	return (type == VAR_ERROR) ? error : "";
}

void Shell::ShellObj::printValue() const
{
	switch (type)
	{
	case VAR_INT:
		cout << int_val << endl;
		break;
	case VAR_DOUBLE:
		cout << dbl_val << endl;
		break;
	case VAR_MATRIX:
		mat_val.print();
		break;
	default:
		cout << "Error: " << error << endl;
	}
}

/// Shell Constructors

Shell::Shell()
{
	lastCmdValid = false;
	lastObj = ShellObj("No last shell object set");
	lastCmd = "";
}

Shell::Shell(const Shell& sh)
{
	variables = map<string, ShellObj>(sh.variables);
	lastCmd = sh.lastCmd;
	lastCmdValid = sh.lastCmdValid;
	lastObj = sh.lastObj;
}

Shell::~Shell()
{
	clearVars();
	lastObj.~ShellObj();
}

/// Private functions

Shell::ShellObj Shell::binaryOperation(const ShellObj left, const ShellObj right, char opr) const
{
	if (left.getType() == VAR_DOUBLE && right.getType() == VAR_DOUBLE)
	{
		switch (opr)
		{
		case '+':
			return ShellObj(left.getDoubleValue() + right.getDoubleValue());
		case '-':
			return ShellObj(left.getDoubleValue() - right.getDoubleValue());
		case '*':
			return ShellObj(left.getDoubleValue() * right.getDoubleValue());
		case '/':
			return ShellObj(left.getDoubleValue() / right.getDoubleValue());
		default:
			return ShellObj("binaryOperation() called on two doubles with invalid operator " + opr);
		}
	}
	else if (left.getType() == VAR_MATRIX && right.getType() == VAR_MATRIX)
	{
		if (opr == '+')
		{
			try
			{
				Matrix<double> add = left.getMatrixValue() + right.getMatrixValue();
				return ShellObj(add);
			}
			catch (MatrixException ex)
			{
				return ShellObj("Error attempting matrix addition: " + (string)ex.what());
			}
		}
		else if (opr == '-')
		{
			try
			{
				Matrix<double> sub = left.getMatrixValue() - right.getMatrixValue();
				return ShellObj(sub);
			}
			catch (MatrixException ex)
			{
				return ShellObj("Error attempting matrix subtraction: " + (string)ex.what());
			}
		}
		else if (opr == '*')
		{
			try
			{
				Matrix<double> mult = left.getMatrixValue() * right.getMatrixValue();
				return ShellObj(mult);
			}
			catch (MatrixException ex)
			{
				return ShellObj("Error attempting matrix multiplication: " + (string)ex.what());
			}
		}
		else if (opr == '|')
		{
			try
			{
				Matrix<double> aug = left.getMatrixValue() | right.getMatrixValue();
				return ShellObj(aug);
			}
			catch (MatrixException ex)
			{
				return ShellObj("Error attempting matrix augmentation: " + (string)ex.what());
			}
		}
		else
			return ShellObj("binaryOperation() called on two matricies with invalid operator type " + opr);
	}
	else if (left.getType() != right.getType())
	{
		// Here we have two operands of disperate types
		if ((left.getType() == VAR_DOUBLE && right.getType() == VAR_INT) || (left.getType() == VAR_INT && right.getType() == VAR_DOUBLE))
		{
			// We first handle the case where one is a double and the other is an int, in which case we'll convert both to doubles and pass them back to binaryOperation()
			if (left.getType() == VAR_INT)
				return binaryOperation(ShellObj((double)left.getIntValue()), right, opr);
			else if (right.getType() == VAR_INT)
				return binaryOperation(left, ShellObj((double)right.getIntValue()), opr);
		}
		else if (left.getType() == VAR_MATRIX || right.getType() == VAR_MATRIX)
		{
			// In this case one of the two objects is a matrix, and hopefully the other is a number otherwise it's an invalid operation
			Matrix<double> mat;
			double num;
			if (left.getType() == VAR_MATRIX)
			{
				mat = left.getMatrixValue();
				if (right.getType() == VAR_DOUBLE)
					num = right.getDoubleValue();
				else if (right.getType() == VAR_INT)
					num = (double)right.getIntValue();
				else
					return ShellObj("binaryOperation() called with an invalid operand");
			}
			else
			{
				mat = right.getMatrixValue();
				if (left.getType() == VAR_DOUBLE)
					num = left.getDoubleValue();
				else if (right.getType() == VAR_INT)
					num = (double)left.getIntValue();
				else
					return ShellObj("binaryOperation() called with an invalid operand");
			}
			if (opr == '*')
			{
				Matrix<double> sclrMult = mat * num;
				return ShellObj(sclrMult);
			}
		}
		else
			return ShellObj("binaryOperation() called with one or more invalid operands");
	}
}

Shell::ShellObj Shell::unaryOperation(const ShellObj obj, char opr) const
{
	if (obj.getType() == VAR_DOUBLE)
	{
		if (opr == '-')
			// Unary minus/negative
			return ShellObj(-1 * obj.getDoubleValue());
		else
			return ShellObj("unaryOperation() called on int with invalid operator type " + opr);
	}
	else if (obj.getType() == VAR_INT)
	{
		if (opr == '-')
			// Unary minus/negative
			return ShellObj(-1 * obj.getIntValue());
		else
			return ShellObj("unaryOperation() called on double with invalid operator type " + opr);
	}
	else if (obj.getType() == VAR_MATRIX)
	{
		if (opr == '~')
		{
			// Transpose
			Matrix<double> trans = ~obj.getMatrixValue();
			return ShellObj(trans);
		}
		else if (opr == '-')
		{
			// Inverse
			Matrix<double> inv = -obj.getMatrixValue();
			return ShellObj(inv);
		}
		else
			return ShellObj("unaryOperation() called on matrix with invalid operator type " + opr);
	}
}

Matrix<double> Shell::parseMatrix(string mstr) const
{
	cleanWhitespace(mstr);
	regex rgx(rgx_matrix);
	if (regex_match(mstr, rgx))
	{
		// Any string which gets this far has is guaranteed to be in the correct matrix-bracket format to be parsed; the only way it might be invalid is if the rows are not all of the same size
		// We first strip off the enclosing brackets
		mstr = mstr.substr(1, mstr.length() - 2);
		// Set up a nested vector to contain the numbers of our matrix
		vector<vector<double>> rowNumVect;
		// Split the string around its semicolons
		vector<string> rowStrVect = splitString(mstr, ';');
		// Iterate through the row strings
		for (int i = 0; i < rowStrVect.size(); ++i)
		{
			string row = rowStrVect[i];
			cleanWhitespace(row);
			// Split the row string around its spaces;
			vector<string> colStrVect = splitString(row, ' ');
			// Set up a vector to store the numbers of this row
			vector<double> colNumVect;
			// Iterate through the column strings; each one should be parseable as a double
			for (int j = 0; j < colStrVect.size(); ++j)
			{
				string num = colStrVect[j];
				//cout << "\tParsing as double: " << num << endl;
				colNumVect.push_back(stod(num));
			}
			// Push the vector for this row onto the matrix vector
			rowNumVect.push_back(colNumVect);
		}
		// If all went well we should have a valid nested vector to pass to the Matrix class constructor!
		try
		{
			Matrix<double> mat(rowNumVect);
			return mat;
		}
		catch (const MatrixException& ex)
		{
			string what = ex.what();
			throw ShellException("Error attempting to parse matrix string: " + what);
		}
	}
	else
		throw ShellException("The following matrix definition string is invalid and could not be parsed:" + mstr);
}

void Shell::clearVars()
{
	if (!variables.empty())
		variables.clear();
}

void Shell::deleteVar(string varname)
{
	if (variables.find(varname) != variables.end())
		variables.erase(varname);
}

void Shell::printVars() const
{
	cout << "STORED VARIABLES:" << endl;
	if (variables.empty())
		cout << "\tNo variables have been saved yet" << endl;
	else
	{
		for (auto itr = variables.begin(); itr != variables.end(); ++itr)
		{
			ShellObj var = itr->second;
			if (var.getType() == VAR_INT)
				cout << "\t" << itr->first << ":\t\t" << var.getIntValue() << endl;
			else if (var.getType() == VAR_DOUBLE)
				cout << "\t" << itr->first << ":\t\t" << var.getDoubleValue() << endl;
			else if (var.getType() == VAR_MATRIX)
			{
				Matrix<double>* mat = &var.getMatrixValue();
				cout << "\t" << itr->first << ":\t\t" << "Matrix; " << mat->getNumRows() << " rows " << mat->getNumsCols() << " columns" << endl;
			}
			else
				cout << "\t" << itr->first << ":\t\t" << "ERROR: " << var.getError() << endl;
		}
	}
	cout << endl;
}

void Shell::storeVar(string target, string varname)
{
	cleanWhitespace(target);
	cleanWhitespace(varname);
	// Validate varname first; it must match the alpha regex and not match the reserved_words regex
	if (regex_match(varname, regex(rgx_alpha)) && !regex_match(varname, regex(rgx_reserved_words)))
	{
		// Validate the target string (it must be parseable as a double, a matrix defined in bracket form, or an existing variable including the $ lastObj symbol)
		if (target == "$")
		{
			if (lastObj.getType() != VAR_ERROR)
			{
				ShellObj var(lastObj);
				deleteVar(varname);
				variables.insert(pair<string, ShellObj>(varname, var));
			}
			else throw ShellException("Error: no last shell object set");
		}
		else if (regex_match(target, regex(rgx_number)))
		{
			// target looks like a number
			try
			{
				double num = stod(target);
				ShellObj var(num);
				deleteVar(varname);
				variables.insert(pair<string, ShellObj>(varname, var));
			}
			catch (const invalid_argument)
			{
				throw ShellException("Error: \"" + target + "\" is an invalid number format (this error should never happen; the regex should catch these)");
			}
			catch (const out_of_range)
			{
				throw ShellException("Error: \"" + target + "\" is too large to be stored in a double (out of range error)");
			}
		}
		else if (regex_match(target, regex(rgx_matrix)))
		{
			// target looks like a matrix
			try
			{
				Matrix<double> m = parseMatrix(target);
				ShellObj var(m);
				deleteVar(varname);
				variables.insert(pair<string, ShellObj>(varname, var));
			}
			catch (const ShellException ex)
			{
				throw ex;
			}
		}
		else if (regex_match(target, regex(rgx_alpha)))
		{
			// target looks like a word; if its a stored variable we save it to varname
			if (variables.find(target) != variables.end())
			{
				ShellObj src = variables[target];
				ShellObj var(src);
				deleteVar(varname);
				variables.insert(pair<string, ShellObj>(varname, var));
			}
			else
				throw ShellException("Error: " + target + " isn't a stored variable");
		}
		else
			throw ShellException("Error: " + target + " is an invalid string for assignment to a variable");
	}
	else
		throw ShellException("Error: \"" + varname + "\" is an invalid variable name (variables may consist only of letters A-Z (any case) and underscores, and can't match any reserved keywords)\nReserved keyword regex is: " + rgx_reserved_words);
}

void Shell::cleanWhitespace(string& str) const
{
	string chars = "\t\n\v\f\r ";
	str.erase(0, str.find_first_not_of(chars));
	str.erase(str.find_last_not_of(chars) + 1);
	str.erase(remove_if(str.begin(), str.end(), &Shell::isWhitespace), str.end());
	string::iterator end = unique(str.begin(), str.end(), &Shell::bothSpaces);
	str.erase(end, str.end());
}

vector<string> Shell::splitString(const string& str, char delim) const
{
	stringstream ss(str);
	string token;
	vector<string> v;
	while (getline(ss, token, delim))
	{
		string c(1, delim);
		if (token != c)
			v.push_back(token);
	}
	return v;
}

// Public functions

bool Shell::processCommand(string cmd)
{
	string unrec = "Error: command not recognized";
	bool success = false;
	if (cmd == "$")
	{
		// cmd is the "$" symbol; print lastObj
		lastObj.printValue();
		success = true;
	}
	else if (regex_match(cmd, regex(rgx_number)))
	{
		// cmd is a bare number; print it (assuming it can be parsed)
		try
		{
			double d = stod(cmd);
			cout << d << endl;
			lastObj = ShellObj(d);
			success = true;
		}
		catch (const invalid_argument)
		{
			cout << "Error: invalid number format" << endl;
		}
		catch (const out_of_range)
		{
			cout << "Error: number is out of range for a double" << endl;
		}
	}
	else if (regex_match(cmd, regex(rgx_matrix)))
	{
		// cmd is a matrix defined in bracket form
		try
		{
			Matrix<double> m = parseMatrix(cmd);
			m.print();
			lastObj = ShellObj(m);
		}
		catch (const ShellException ex)
		{
			cout << ex.what() << endl;
		}
		success = true;
	}
	else if (regex_match(cmd, regex(rgx_alpha)))
	{
		// cmd is a word (matching [a-zA-Z_]), and could either be a no-parameter command or a bare variable
		if (regex_match(cmd, regex(rgx_singlet_commands)))
		{
			success = true;
			if (cmd == "help")
				help("");
			else if (cmd == "vars")
				printVars();
			else if (cmd == "exit")
				cout << "exit command handled by processCommand() (this shouldn't happen)" << endl;
		}
		else
		{
			// cmd is either a variable or an unrecognized command
			if (variables.find(cmd) != variables.end())
			{
				cout << cmd << " = ";
				if (variables[cmd].getType() == VAR_MATRIX)
					cout << endl;
				variables[cmd].printValue();
				lastObj = variables[cmd];
				success = true;
			}
			else
				cout << unrec << endl;
		}
	}
	else if (regex_match(cmd, regex(rgx_assignment)))
	{
		// cmd is an assignment operation (hopefully, a valid one)
		vector<string> s = splitString(cmd, '=');
		if (s.size() == 2)
		{
			try
			{
				storeVar(s[1], s[0]);
				lastObj = variables[s[0]];
				success = true;
			}
			catch (ShellException ex)
			{
				cout << ex.what() << endl;
			}
		}
		else
			cout << "Error: invalid assignment command (split into more than 2 tokens?)" << endl;
	}
	else if (cmd.find_first_of(" ") != string::npos)
	{
		string param = cmd.substr(cmd.find_first_of(' ') + 1);
		cmd = cmd.substr(0, cmd.find_first_of(' '));

		if (cmd == "test_int" || cmd == "test_dec" || cmd == "test_num" || cmd == "test_alpha" || cmd == "test_matrix" || cmd == "test_ass")
		{
			regexTesting(cmd, param);
			success = true;
		}
		else if (cmd == "delvar")
		{
			if (regex_match(param, regex(rgx_alpha)))
			{
				deleteVar(param);
			}
			else
				cout << "Error: \"" << param << "\" is not a valid variable name" << endl;
		}
		else
			cout << unrec << endl;
	}
	else
		cout << unrec << endl;
	if (success)
		lastCmd = cmd;
	return success;
}

/*
bool Shell::processCommand(string cmd)
{
	string unrec = "Error: command not recognized";
	bool success = false;
	cleanWhitespace(cmd);
	if (cmd == "")
	{
		// If cmd is just empty string, we do nothing
	}
	else if (regex_match(cmd, regex("^\S+$")))
	{
		// Cmd contains no spaces

		// first, check if cmd matches "$"; if so, call processCommand(lastCmd) (essentially repeating the last successful command similar to the "!!" operation in unix shells)
		// first, check if cmd matches a no-parameter command ("help" is the only one I can think of right now)
		// then check if cmd matches a variable stored in the variables map; if so, print it (and update lastObj)
		// then check if cmd matches the number regex; if so, generate a shellobj and print the number (and update lastObj)
		// then check if cmd matches the matrix-definition regex; if so, generate a shellobj and print the matrix (and update lastObj)
		// otherwise, print the unrecognized command message

	}
	else
	{
		// cmd has multiple parts; we need to tokenize it by splitting on spaces
		vector<string> tokens;
		if (cmd == "unary operation on number")
		{
			// this block is for if the first token matches one of the unary operations we can perform on a number
			// the next token MUST be either a number, or a stored variable which is a double or int...currently we're not going to support nested calculations in operation calls

			// if the next token regex matches a number, perform the operation and print result
			// if the next token regex matches a matrix definition, print error saying the oepration can only be performed on a number
			// if the next token is alphabetical, we check the variables map for it
				// if a matching variable is stored and is a number, perform the operation and print result
				// if a matching variable is stored and is a matrix, print error saying the oepration can only be performed on a number
				// if no variable matches the string, print error saying variable cannot be found
			// if the next token is anything else, print the unrecognized command message
		}
		else if (cmd == "unary operation on matrix")
		{
			// this block is for if the first token matches one of the unary operations we can perform on a matrix
			// the next token MUST be either a matrix defined by bracket notation, or a stored variable which is a matrix...currently we're not going to support nested calculations in operation calls

			// if the next token regex matches a number, print error saying the oepration can only be performed on a matrix
			// if the next token regex matches a matrix definition, perform the operation and print result
			// if the next token is alphabetical, we check the variables map for it
				// if a matching variable is stored and is a number, print error saying the oepration can only be performed on a matrix
				// if a matching variable is stored and is a matrix, perform the operation and print result
				// if no variable matches the string, print error saying variable cannot be found
			// if the next token is anything else, print the unrecognized command message
		}
		else if (cmd == "number regex")
		{
			// this block is for if the first token matches a number

			if (cmd == "=")
			{
				// this block is for if the next token is an equals sign (assignment operator)
			}
			else if (cmd == "operator")
			{
				// this block is for if the next token is an operator
			}
			else
			{
				// print the unrecognized command message
			}
		}
	}
	return success;
}
*/

void Shell::executeScript()
{

}

void Shell::prompt()
{
	string prmpt = "> ";
	string cmd;
	do
	{
		// clear the command buffer
		cmd = "";
		// print the prompt string
		cout << prmpt;
		// read one line of keyboard input into the command buffer
		getline(cin, cmd);
		// remove all leading and trailing whitespace from the command buffer, strip all non-space whitespace characters, and replace any repeated spaces with a single space
		cleanWhitespace(cmd);
		// if command isn't the empty string or "exit" pass it to processCommand() to be handled
		if (cmd != "" && cmd != "exit")
			lastCmdValid = processCommand(cmd);
	} while (cmd != "exit");
}

void Shell::regexTesting(string cmd, string param)
{
	lastCmdValid = true;
	if (cmd == "test_int")
	{
		regex rgx(rgx_integer);
		if (regex_match(param, rgx))
			cout << "True" << endl;
		else
			cout << "False" << endl;
	}
	else if (cmd == "test_dec")
	{
		regex rgx(rgx_decimal);
		if (regex_match(param, rgx))
			cout << "True" << endl;
		else
			cout << "False" << endl;
	}
	else if (cmd == "test_num")
	{
		regex rgx(rgx_number);
		if (regex_match(param, rgx))
			cout << "True" << endl;
		else
			cout << "False" << endl;
	}
	else if (cmd == "test_alpha")
	{
		regex rgx(rgx_alpha);
		if (regex_match(param, rgx))
			cout << "True" << endl;
		else
			cout << "False" << endl;
	}
	else if (cmd == "test_matrix")
	{
		regex rgx(rgx_matrix);
		if (regex_match(param, rgx))
			cout << "True" << endl;
		else
			cout << "False" << endl;
	}
	else if (cmd == "test_ass")
	{
		regex rgx(rgx_assignment);
		if (regex_match(param, rgx))
			cout << "True" << endl;
		else
			cout << "False" << endl;
	}
	else
	{
		lastCmdValid = false;
		cout << "Invalid command" << endl;
	}
}

void Shell::help(string com = "") const
{
	string defaultMsg = string() + "Eigenshell - A basic calculator shell for numeric calculations and matrix algebra\n" +
		"Available commands (brackets denote required parameters; parentheses denote optional parameters):\n" +
		"exit:" +
		"\tExit the shell and quit\n" +
		"help (command):\n" +
		"\tDisplay this message, or help regarding a particular command or function\n" +
		"vars:\n" +
		"\tPrint a list of currently stores variables\n" +
		"$:\n" +
		"\tThe $ symbol may be used as a variable to stand in for the last valid object (number, matrix, or variable) printed by the shell\n" +
		"\n";
	if (com == "")
		cout << defaultMsg << endl;
	else
		cout << "No help available for \"" << com << "\"" << endl;
}

void Shell::testFunc() const
{
	//Matrix<double> test = parseMatrix("[1 22; 3 0.4; 420 13.37]");
	//test.print();
	/*double b = 132.55;
	void* ptr = &b;
	cout << "Double b: " << b << endl;
	cout << "void* b raw: " << ptr << endl;
	cout << "void* b static-cast de-ref: " << *static_cast<double*>(ptr) << endl;
	cout << "void* b normal cast de-ref: " << *(double*)ptr << endl;
	*/
	
	regex ass(rgx_assignment);
}

#endif