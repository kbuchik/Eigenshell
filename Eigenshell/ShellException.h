#pragma once
#ifndef SHELL_EXCEPTION_H
#define SHELL_EXCEPTION_H

#include <exception>
#include <string>

class ShellException : public std::exception
{
protected:
	std::string errmsg;

public:
	ShellException(const std::string& msg) : errmsg(msg) {}
	~ShellException() {}
	virtual const char* what() const { return errmsg.c_str(); }
};

#endif;