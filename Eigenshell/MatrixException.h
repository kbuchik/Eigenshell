#pragma once
#ifndef MATRIX_EXCEPTION_H
#define MATRIX_EXCEPTION_H

#include <exception>
#include <string>

class MatrixException : public std::exception
{
protected:
	std::string errmsg;

public:
	MatrixException(const std::string& msg) : errmsg(msg) {}
	~MatrixException() {}
	virtual const char* what() const { return errmsg.c_str(); }
};

#endif;