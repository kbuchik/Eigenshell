#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include "MatrixException.h"

using namespace std;

#pragma once
class IntMatrix
{
private:
	size_t rows = 0;
	size_t cols = 0;
	int** m = nullptr;

	// Destructor helper
	void destroyMatrix();

public:
	IntMatrix() {};
	IntMatrix(int, int);
	IntMatrix(const IntMatrix&);
	IntMatrix(const vector<vector<int>>);
	~IntMatrix();

	bool isRow() const { return rows == 1; }
	bool isColumn() const { return cols == 1; }
	bool isSquare() const { return cols == rows; }
	bool isDiagonal() const;
	bool isScalar() const;
	bool isIdentity() const;
	bool isUpperTriangular() const;
	bool isLowerTriangular() const;
	bool isNull() const;

	// Accessors, value mutator, print function
	int getNumRows() const { return cols; }
	int getNumsCols() const { return rows; }
	int getValue(int i, int j) const;
	void setValue(int i, int j, int value);
	void print() const;

	// Determinant
	int det() const;
	// Returns the i,jth minor of this matrix
	IntMatrix minor(int i, int j) const;

	// Assignment operator
	IntMatrix& operator=(const IntMatrix&);
	// Equality operator
	bool operator==(const IntMatrix&) const;
	// Matrix addition operator
	IntMatrix operator+(const IntMatrix&) const;
	// Matrix addition assignment operator
	IntMatrix& operator+=(const IntMatrix&);
	// Matrix subtraction operator
	IntMatrix operator-(const IntMatrix&) const;
	// Matrix subtraction assignment operator
	IntMatrix& operator-=(const IntMatrix&);
	// Scalar multiplication operator (right-hand)
	IntMatrix operator*(int) const;
	// Matrix multiplicaton operator
	IntMatrix operator*(const IntMatrix&) const;
	// Transpose operator
	IntMatrix operator~() const;
	// Inverse operator
	IntMatrix operator-() const;
};

// Free function overload for left-hand scalar multiplication
IntMatrix operator*(int, const IntMatrix&);