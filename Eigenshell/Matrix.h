#pragma once
#ifndef MATRIX_H
#define MATRIX_H

#include <cassert>
#include <iostream>
#include <string>
#include <typeinfo>
#include <type_traits>
#include <vector>
#include "MatrixException.h"

using namespace std;

template <typename T>
class Matrix
{
private:
	size_t rows = 0;
	size_t cols = 0;
	T** m = nullptr;

	// Destructor helper
	void destroyMatrix();													// Iterates through the "m" matrix pointer/array and frees its memory

	/// Elementary matrix operations
	void rowSwap(int, int);													// Swap row A and B (for A,B < rows)
	void rowMult(int, T);													// Multiply the given row by a scalar value
	void rowAdd(int, int, T);												// Replace a row by its sum with the scalar multiple of another row

public:
	Matrix();																// Default constructor (initializes a 1x1 matrix with 0 as value)
	Matrix(int, int);														// Initializes an MxN matrix with zeros
	Matrix(const Matrix&);													// Copy constructor
	Matrix(const vector<vector<T>>);										// Vector constructor
	~Matrix();																// Destructor (calls destroyMatrix())

	/// Boolean property functions
	bool isInt() const { return sizeof(T) == sizeof(int); }
	bool isDouble() const { return sizeof(T) == sizeof(double); }
	bool isRow() const { return rows == 1; }
	bool isColumn() const { return cols == 1; }
	bool isSquare() const { return cols == rows; }
	bool isDiagonal() const;
	bool isScalar() const;
	bool isIdentity() const;
	bool isUpperTriangular() const;
	bool isLowerTriangular() const;
	bool isNull() const;

	/// Accessors, value mutator, print function
	int getNumRows() const { return cols; }
	int getNumsCols() const { return rows; }
	T getValue(int i, int j) const;
	void setValue(int i, int j, int value);
	void setValue(int i, int j, double value);
	void print() const;
	string toString() const;
	string getTypeName() const;

	/// Operator overloads
	Matrix& operator=(const Matrix&);										// Assignment operator
	bool operator==(const Matrix&) const;									// Equality operator
	Matrix operator+(const Matrix&) const;									// Matrix addition operator
	Matrix& operator+=(const Matrix&);										// Matrix addition assignment operator
	Matrix operator-(const Matrix&) const;									// Matrix subtraction operator
	Matrix& operator-=(const Matrix&);										// Matrix subtraction assignment operator
	Matrix operator*(int) const;											// Scalar multiplication operator for ints (right-hand)
	Matrix operator*(double) const;											// Scalar multiplication operator for doubles (right-hand)
	Matrix operator*(const Matrix&) const;									// Matrix multiplication operator
	Matrix operator~() const;												// Transpose operator
	Matrix operator-() const;												// Inverse operator	(will always return a double-type matrix)
	Matrix operator|(const Matrix&) const;									// Augment (column append) operator

	/// Other operations
	double det() const;														// Calculates the determinant of this matrix (recursively)
	Matrix minor(int i, int j) const;										// Returns a minor of this matrix (a matrix of n - 1 size in both dimensions which excludes the values from row i or column j)
	void rref();															// Transforms this matrix to reduced row echelon form using Gaussian elimination (in-place)
	Matrix<double> get_rref() const;										// Returns the reduced row echelon form of this matrix

	/// Utility functions
	Matrix identity(int n) const;											// Static function which returns the identity matrix of the given size
	Matrix<double> doubleConvert() const;									// Returns a copy of this matrix in double form (only works on int matricies; an exception is thrown otherwise)
};

// Free function overload for left-hand scalar multiplication
//Matrix operator*(int, const Matrix&);

// Because this is a template class, the .cpp file must be included here, otherwise the linker won't be able to find our function definitions
#include "Matrix.cpp"
#endif