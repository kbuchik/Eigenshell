#include "IntMatrix.h"

IntMatrix::IntMatrix(int x, int y)
{
	if (x > 0 && y > 0)
	{
		rows = x;
		cols = y;
		m = new int* [rows]();
		for (int i = 0; i < rows; ++i)
		{
			m[i] = new int[cols]();
			for (int j = 0; j < cols; ++j)
				m[i][j] = 0;
		}
	}
	else throw MatrixException("Matrix dimensions must be positive non-zero integers");
}

IntMatrix::IntMatrix(const IntMatrix& mat)
{
	rows = mat.rows;
	cols = mat.cols;
	m = new int* [rows]();
	for (int i = 0; i < rows; ++i)
	{
		m[i] = new int[cols]();
		for (int j = 0; j < cols; ++j)
			m[i][j] = mat.m[i][j];
	}
}

IntMatrix::IntMatrix(const vector<vector<int>> v)
{	
	rows = v.size();
	assert(rows > 0);
	cols = v[0].size();
	assert(cols > 0);
	m = new int* [rows]();
	for (int i = 0; i < rows; ++i)
	{
		if (v[i].size() != cols)
			throw MatrixException("Failed initializing matrix from vectors: all rows (sub-vectors) must be equal in size");
		m[i] = new int[cols]();
		for (int j = 0; j < cols; ++j)
			m[i][j] = v[i][j];
	}
}

IntMatrix::~IntMatrix()
{
	destroyMatrix();
}

void IntMatrix::destroyMatrix()
{
	for (int i = 0; i < rows; ++i)
		delete[] m[i];
	delete[] m;
}

bool IntMatrix::isDiagonal() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (i != j && m[i][j] != 0)
				return false;
	return true;
}

bool IntMatrix::isScalar() const
{
	int x = m[0][0];
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
		{
			if (i == j)
				if (m[i][j] != x)
					return false;
				else
					if (m[i][j] != 0)
						return false;
		}
	return true;
}

bool IntMatrix::isIdentity() const
{
	if (rows != cols)
		return false;
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
		{
			if (i == j)
				if (m[i][j] != 1)
					return false;
				else
					if (m[i][j] != 0)
						return false;
		}
	return true;
}

bool IntMatrix::isUpperTriangular() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (i > j && m[i][j] != 0)
				return false;
	return true;
}

bool IntMatrix::isLowerTriangular() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (i < j && m[i][j] != 0)
				return false;
	return true;
}

bool IntMatrix::isNull() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (m[i][j] != 0)
				return false;
	return true;
}

int IntMatrix::getValue(int i, int j) const
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
		return m[i][j];
	else throw MatrixException("Cell " + to_string(i) + "," + to_string(j) + " is out of bounds for a " + to_string(rows) + "x" + to_string(cols) + " matrix");
}

void IntMatrix::setValue(int i, int j, int value)
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
		m[i][j] = value;
	else throw MatrixException("Cell " + to_string(i) + "," + to_string(j) + " is out of bounds for a " + to_string(rows) + "x" + to_string(cols) + " matrix");
}

void IntMatrix::print() const
{
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			cout << "\t" << m[i][j];
		cout << endl;
	}
}

int IntMatrix::det() const
{
	if (rows == cols)
	{
		if (rows == 1)
			return m[0][0];
		else
		{
			int d = 0;
			int s = 1;
			for (int j = 0; j < cols; ++j)
			{
				d += s * m[0][j] * this->minor(0, j).det();
				s = -1 * s;
			}
			return d;
		}
	}
	else throw new MatrixException("Cannot calculate determinant for a non-square matrix");
}

IntMatrix IntMatrix::minor(int i, int j) const
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
	{
		if (rows == 1 || cols == 1)
			throw MatrixException("Cannot calculate a minor on a 1-dimensional matrix");
		else
		{
			IntMatrix min(rows - 1, cols - 1);
			// 'r' and 'c' will be the indices used to traverse this matrix, while 'm' and 'n' will be used to populate the minor matrix
			int m = 0;
			int n = 0;
			for (int r = 0; r < rows; ++r)
			{
				if (r != i)
				{
					for (int c = 0; c < cols; ++c)
					{
						if (c != j)
						{
							min.m[m][n] = this->m[r][c];
							++n;
						}
					}
					++m;
					n = 0;
				}
			}
			return min;
		}
	}
	else throw MatrixException("Error calculating " + to_string(i) + "," + to_string(j) + " minor on matrix with " + to_string(rows) + " rows and " + to_string(cols) + " columns (out of bounds error)");
}

IntMatrix& IntMatrix::operator=(const IntMatrix& mat)
{
	destroyMatrix();
	rows = mat.rows;
	cols = mat.cols;
	m = new int* [rows]();
	for (int i = 0; i < rows; ++i)
	{
		m[i] = new int[cols]();
		for (int j = 0; j < cols; ++j)
			m[i][j] = mat.m[i][j];
	}
	return *this;
}

bool IntMatrix::operator==(const IntMatrix& opr) const
{
	if (rows == opr.rows && cols == opr.cols)
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				if (m[i][j] != opr.m[i][j])
					return false;
		return true;
	}
	else return false;
}

IntMatrix IntMatrix::operator+(const IntMatrix& opr) const
{
	if (rows == opr.rows && cols == opr.cols)
	{
		IntMatrix sum(rows, cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				sum.m[i][j] = m[i][j] + opr.m[i][j];
		return sum;
	}
	else throw MatrixException("Cannot add two matricies of differing sizes");
}

IntMatrix& IntMatrix::operator+=(const IntMatrix& opr)
{
	if (rows == opr.rows && cols == opr.cols)
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				m[i][j] += opr.m[i][j];
		return *this;
	}
	else throw MatrixException("Cannot add two matricies of differing sizes");
}

IntMatrix IntMatrix::operator-(const IntMatrix& opr) const
{
	if (rows == opr.rows && cols == opr.cols)
	{
		IntMatrix diff(rows, cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				diff.m[i][j] = m[i][j] - opr.m[i][j];
		return diff;
	}
	else throw MatrixException("Cannot subtract two matricies of differing sizes");
}

IntMatrix& IntMatrix::operator-=(const IntMatrix& opr)
{
	if (rows == opr.rows && cols == opr.cols)
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				m[i][j] -= opr.m[i][j];
		return *this;
	}
	else throw MatrixException("Cannot subtract two matricies of differing sizes");
}

IntMatrix IntMatrix::operator*(int opr) const
{
	IntMatrix mult(rows, cols);
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			mult.m[i][j] = m[i][j] * opr;
	return mult;
}

// Definition for external left-hand scalar multiplication operator overload
IntMatrix operator*(int x, const IntMatrix& y)
{
	return y * x;
}

IntMatrix IntMatrix::operator*(const IntMatrix& opr) const
{
	if (cols == opr.rows)
	{
		IntMatrix mult(rows, opr.cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < opr.cols; ++j)
			{
				int sum = 0;
				for (int k = 0; k < cols; ++k)
					sum += m[i][k] * opr.m[k][j];
				mult.m[i][j] = sum;
			}
		return mult;
	}
	else throw MatrixException("Matrix multiplication error: the number of columns in the first (lvalue) matrix must equal the number of rows of the second (rvalue) matrix");
}

IntMatrix IntMatrix::operator~() const
{
	IntMatrix trans(cols, rows);
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			trans.m[j][i] = m[i][j];
	return trans;
}

// Ugh...the inverse operator can't be implemented for a matrix that can only store integers
IntMatrix IntMatrix::operator-() const
{
	IntMatrix inv(rows, cols);

	return inv;
}