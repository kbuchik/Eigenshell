// Eigenshell.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Matrix.h"
#include "MatrixException.h"
#include "Shell.h";

using namespace std;

void testMatrixOps()
{
    try
    {
        vector<vector<int>> av{
            {1, 5, 15},
            {0, 4, -1},
            {16, 25, -69}
        };
        vector<vector<int>> aav{
            {1, 2, -1, -4},
            {2, 3, -1, -11},
            {-2, 0, -3, 22}
        };
        
        vector<vector<int>> bv{
            {1, 0, 0, 0},
            {5, 6, 0, 0},
            {7, 18274, 7, 0},
            {122, 5, 225, 4}
        };
        vector<vector<double>> cv{
            {0.7, 1.25, 3.692},
            {5.55, 4.13, 666},
            {1337, 12.43, 0.01}
        };
        vector<vector<double>> dv{
            {4},
            {3},
            {1}
        };
        vector<vector<double>> ev{
            {4, 7},
            {2, 6}
        };
        
        Matrix<int> a(av);
        cout << "Matrix A:" << endl;
        a.print();
        Matrix<double> b = a.doubleConvert();
        cout << "Matrix A double-converted:" << endl;
        b.print();
        /*
        IntMatrix a(av);
        IntMatrix b(bv);
        cout << "Matrix A:" << endl;
        a.print();
        cout << "Upper triangular: " << a.isUpperTriangular() << endl;
        cout << "Lower triangular: " << a.isLowerTriangular() << endl;
        cout << "Matrix B:" << endl;
        b.print();
        cout << "Upper triangular: " << b.isUpperTriangular() << endl;
        cout << "Lower triangular: " << b.isLowerTriangular() << endl;
        cout << "2 * A:" << endl;
        (2 * a).print();
        
        //cout << "Determinant of A: " << a.det() << endl;
        cout << "Matrix B: " << endl;
        b.print();
        cout << "Matrix AB: " << endl;
        (a * b).print();
        */
    }
    catch (const MatrixException& ex)
    {
        cerr << ex.what() << endl;
    }
}

int main()
{
    cout << "Eigenshell initialized" << endl;
    //testMatrixOps();
    Shell sh;
    sh.prompt();
    //sh.testFunc();
}