#ifndef SHELL_H
#define SHELL_H

#pragma once

#include <iostream>
#include <iterator>
#include <map>
#include <regex>
#include <string>
#include <sstream>
#include <vector>
#include "Matrix.h"
#include "ShellException.h"

using namespace std;

class Shell
{
private:
	
	// Type identifer for ShellObj objects
	enum vartype { VAR_INT, VAR_DOUBLE, VAR_MATRIX, VAR_ERROR };
	// The "ERROR" type is used when attempting to construct a shellvar fails, and should never actually be stored in the variables map
	// Also, all matricies will be of double type...trying to make int matricies and worrying about checking and converting them is just TOO annoying

	// Regex strings to be used in expression parsing
	const string rgx_alpha = "^[a-zA-Z_]+$";
	const string rgx_assignment = "^[a-zA-Z_]+\\s?=\\s?((-?((\\d+(\\.\\d*)?)|(\\.\\d+)))|\\$|[a-zA-Z_]+|(\\[\\s?((-?((\\d+(\\.\\d*)?)|(\\.\\d+)))(\\s(-?((\\d+(\\.\\d*)?)|(\\.\\d+))))*)(\\s?;\\s(-?((\\d+(\\.\\d*)?)|(\\.\\d+)))(\\s(-?((\\d+(\\.\\d*)?)|(\\.\\d+))))*)*\\s?\\]))$";
	const string rgx_decimal_token = "-?((\\d+\\.\\d+)|(\\.\\d+))";
	const string rgx_decimal = "^" + rgx_decimal_token + "$";
	const string rgx_integer = "^-?\\d+$";
	const string rgx_matrix_token = "\\[\\s?((-?((\\d+(\\.\\d*)?)|(\\.\\d+)))(\\s(-?((\\d+(\\.\\d*)?)|(\\.\\d+))))*)(\\s?;\\s(-?((\\d+(\\.\\d*)?)|(\\.\\d+)))(\\s(-?((\\d+(\\.\\d*)?)|(\\.\\d+))))*)*\\s?\\]";
	const string rgx_matrix = "^" + rgx_matrix_token + "$";
	const string rgx_number_token = "-?((\\d+(\\.\\d*)?)|(\\.\\d+))";
	const string rgx_number = "^" + rgx_number_token + "$";
	const string rgx_singlet_commands = "^(exit|help|vars)$";
	const string rgx_reserved_words = "^(delvar|exit|help|sqrt|test_alpha|test_ass|test_dec|test_int|test_matrix|test_num|vars)$";
	const string rgx_regex_commands = "^(test_alpha|test_ass|test_dec|test_int|test_matrix|test_num)$";
	const string rgx_matrix_commands = "^(size|isRow|isColumn|isRectangular|isSquare|isDiagonal|isScalar|isIdentity|isUpperTriangular|isLowerTriangular|isNull|isSymmetric|isSkewSymmetric|areEqual|det|isSingular)$";

	// Container class to manage variables used in the shell, which may be either numbers or matricies
	class ShellObj
	{
	private:
		vartype type;
		string error;

		union {
			int int_val;
			double dbl_val;
			Matrix<double> mat_val;
		};
		
	public:
		ShellObj();
		ShellObj(const ShellObj&);
		ShellObj(int);
		ShellObj(double);
		ShellObj(Matrix<double>&);
		ShellObj(string);
		~ShellObj();
		ShellObj& operator=(const ShellObj&);

		vartype getType() const { return type; }
		int getIntValue() const { return int_val; }
		double getDoubleValue() const { return dbl_val; }
		Matrix<double> getMatrixValue() const { return mat_val; }
		string getError() const;
		void printValue() const;
	};

	map<string, ShellObj> variables;
	vector<string> cmdBuffer;
	string lastCmd;
	ShellObj lastObj;
	bool lastCmdValid;

	// Attempt to perform the binary operation denoted by the operator character on the values stored in the two shellvar parameters, and return the result (an error string if the operation in invalid)
	ShellObj binaryOperation(const ShellObj, const ShellObj, char) const;
	// Attempt to perform the unary operation denoted by the operator character on the value stored in the ShellObj parameter
	ShellObj unaryOperation(const ShellObj, char) const;

	/// Expression evaluation and parsing functions

	// Attempts to evaluate the (currently, numerical only) expression defined by the string and returns the result (stored as double) in a shellobj (or an error shellobj if the expression could not be parsed)
	ShellObj evalExpression(string) const;
	// This will parse a bracket notation string defining a matrix ("[ x x ; y y ; z z ]"), construct a matrix object of the appropriate type, and return a shellobj struct containing that variable
	Matrix<double> parseMatrix(string) const;

	/// Variable map functions

	// Clears the variables map
	void clearVars();
	// Deletes the given variable from the variables map
	void deleteVar(string);
	// Prints the contents of the variables map
	void printVars() const;
	// Attempts to parse a string (1st param) into (currently only) a double, and save it in the variables map under the given name (2nd param)
	void storeVar(string, string);

	/// String-processing helper functions

	// Strip leading and trailing whitespace from a string, remove all non-space whitespace characters, and reduce repeated spaces to a single space (done in-place, so string is passed by reference)
	void cleanWhitespace(string&) const;
	// Splits a string by spaces and returns a vector of the resulting tokens
	vector<string> splitString(const string&, char) const;
	// Unary predicate function for cleanWhitespace(); returns true if both characters are spaces
	static bool bothSpaces(char l, char r) { return (l == r) && (l == ' '); };
	// Unary predicate function for cleanWhitespace(); returns true if the passed character is a non-space whitespace character ("\n\r\t\v\f")
	static bool isWhitespace(unsigned char c) { return (c == '\n' || c == '\r' || c == '\t' || c == '\v' || c == '\f'); };

public:
	Shell();
	Shell(const Shell&);
	~Shell();

	// Parses and executes a single command
	bool processCommand(string);
	// Iterates through a file and executes each line using processCommand()
	void executeScript();
	// Prompts for commands on a loop until "exit" is entered
	void prompt();
	// Processes commands for testing my regular expressions
	void regexTesting(string, string);
	// Prints help for a specific command (or if the parameter is null or empty string, prints list of acceptable commands)
	void help(string) const;
	// Function that will be used for testing various shit I write
	void testFunc() const;
};

#endif;