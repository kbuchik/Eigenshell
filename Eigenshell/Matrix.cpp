#pragma once
#ifndef MATRIX_CPP
#define MATRIX_CPP

// #ifndef MUST be used here, otherwise this line will cause an infinite recursive loop of includes in the preprocesser
#include "Matrix.h"

template <typename T>
Matrix<T>::Matrix()
{
	if constexpr (is_same_v<T, int> || is_same_v<T, double>)
	{
		rows = cols = 1;
		m = new T * [1]();
		m[0] = new T[1]();
		m[0][0] = 0;
	}
	else throw MatrixException("Matrix class only supports int and double types");	
}

template <typename T>
Matrix<T>::Matrix(int x, int y)
{
	if constexpr (is_same_v<T, int> || is_same_v<T, double>)
	{
		rows = x;
		cols = y;
		m = new T * [rows]();
		for (int i = 0; i < rows; ++i)
		{
			m[i] = new T[cols]();
			for (int j = 0; j < cols; ++j)
				m[i][j] = 0;
		}
	}
	else throw MatrixException("Matrix class only supports int and double types");
}

template <typename T>
Matrix<T>::Matrix(const Matrix& src)
{
	if constexpr (is_same_v<T, int> || is_same_v<T, double>)
	{
		cols = src.cols;
		rows = src.rows;
		m = new T * [rows]();
		for (int i = 0; i < rows; ++i)
		{
			m[i] = new T[cols]();
			for (int j = 0; j < cols; ++j)
				m[i][j] = src.m[i][j];
		}
		/*
		if (typeid(m).name() == src.getTypeName())
		{
			cols = src.cols;
			rows = src.rows;
			m = new T * [rows]();
			for (int i = 0; i < rows; ++i)
			{
				m[i] = new T[cols]();
				for (int j = 0; j < cols; ++j)
					m[i][j] = src.m[i][j];
			}
		}
		else
		{
			cols = src.cols;
			rows = src.rows;
			m = new T * [rows]();
			if constexpr (is_same_v<T, int>)
			{
				// We're dealing with a declared int matrix being passed a double matrix
				for (int i = 0; i < rows; ++i)
				{
					m[i] = new T[cols]();
					for (int j = 0; j < cols; ++j)
						m[i][j] = int(src.m[i][j]);
				}
			}
			else if constexpr (is_same_v<T, double>)
			{
				// We're dealing with a declared double matrix being passed an int matrix
				for (int i = 0; i < rows; ++i)
				{
					m[i] = new T[cols]();
					for (int j = 0; j < cols; ++j)
						m[i][j] = double(src.m[i][j]);
				}
			}
			else throw MatrixException("WHAT THE HAI?!");
		}
		*/
	}
	else throw MatrixException("Matrix class only supports int and double types");
}

template <typename T>
Matrix<T>::Matrix(const vector<vector<T>> v)
{
	if constexpr (is_same_v<T, int> || is_same_v<T, double>)
	{
		rows = v.size();
		assert(rows > 0);
		cols = v[0].size();
		assert(cols > 0);
		m = new T* [rows]();
		for (int i = 0; i < rows; ++i)
		{
			if (v[i].size() != cols)
				throw MatrixException("Failed initializing matrix from vectors: all rows (sub-vectors) must be equal in size");
			m[i] = new T[cols]();
			for (int j = 0; j < cols; ++j)
				m[i][j] = v[i][j];
		}
	}
	else throw MatrixException("Matrix class only supports int and double types");
}

template <typename T>
Matrix<T>::~Matrix()
{
	destroyMatrix();
}

template <typename T>
void Matrix<T>::destroyMatrix()
{
	for (int i = 0; i < rows; ++i)
		delete[] m[i];
	delete[] m;
}

template <typename T>
void Matrix<T>::rowSwap(int a, int b)
{
	if (a >= 0 && a < rows && b >= 0 && b < rows)
	{
		for (int i = 0; i < cols; ++i)
		{
			T tmp = m[a][i];
			m[a][i] = m[b][i];
			m[b][i] = tmp;
		}
	}
	else throw MatrixException("rowSwap() called with an invalid row number");
}

template <typename T>
void Matrix<T>::rowMult(int a, T n)
{
	if (a >= 0 && a < rows)
		for (int i = 0; i < cols; ++i)
			m[a][i] *= n;
	else throw MatrixException("rowMult() called with an invalid row number");
}

template <typename T>
void Matrix<T>::rowAdd(int a, int b, T n)
{
	// Replaces row a by its sum with the scalar multiple of row b
	if (a >= 0 && a < rows && b >= 0 && b < rows)
		for (int i = 0; i < cols; ++i)
			m[a][i] += n * m[b][i];
	else throw MatrixException("rowAdd() called with an invalid row number");
}

template <typename T>
bool Matrix<T>::isDiagonal() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (i != j && m[i][j] != 0)
				return false;
	return true;
}

template <typename T>
bool Matrix<T>::isScalar() const
{
	int x = m[0][0];
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
		{
			if (i == j)
				if (m[i][j] != x)
					return false;
				else
					if (m[i][j] != 0)
						return false;
		}
	return true;
}

template <typename T>
bool Matrix<T>::isIdentity() const
{
	if (rows != cols)
		return false;
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
		{
			if (i == j)
				if (m[i][j] != 1)
					return false;
				else
					if (m[i][j] != 0)
						return false;
		}
	return true;
}

template <typename T>
bool Matrix<T>::isUpperTriangular() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (i > j && m[i][j] != 0)
				return false;
	return true;
}

template <typename T>
bool Matrix<T>::isLowerTriangular() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (i < j && m[i][j] != 0)
				return false;
	return true;
}

template <typename T>
bool Matrix<T>::isNull() const
{
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (m[i][j] != 0)
				return false;
	return true;
}

template <typename T>
T Matrix<T>::getValue(int i, int j) const
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
		return m[i][j];
	else throw MatrixException("Cell " + to_string(i) + "," + to_string(j) + " is out of bounds for a " + to_string(rows) + "x" + to_string(cols) + " matrix");
}

template <typename T>
void Matrix<T>::setValue(int i, int j, int value)
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
		if constexpr (is_same_v<T, double>)
			m[i][j] = double(value);
		else
			m[i][j] = value;
	else throw MatrixException("Cell " + to_string(i) + "," + to_string(j) + " is out of bounds for a " + to_string(rows) + "x" + to_string(cols) + " matrix");
}

template <typename T>
void Matrix<T>::setValue(int i, int j, double value)
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
		if constexpr (is_same_v<T, double>)
			m[i][j] = value;
		else
			m[i][j] = int(value);
	else throw MatrixException("Cell " + to_string(i) + "," + to_string(j) + " is out of bounds for a " + to_string(rows) + "x" + to_string(cols) + " matrix");
}

template <typename T>
void Matrix<T>::print() const
{
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			cout << "\t" << m[i][j];
		cout << endl;
	}
}

template <typename T>
string Matrix<T>::toString() const
{
	string p = "";
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			p += m[i][j];
		p += "\n";
	}
	return p;
}

template <typename T>
string Matrix<T>::getTypeName() const
{
	return string(typeid(m).name());
}

template <typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix& mat)
{
	if (typeid(m).name() == mat.getTypeName())
	{
		rows = mat.rows;
		cols = mat.cols;
		m = new T* [rows]();
		for (int i = 0; i < rows; ++i)
		{
			m[i] = new T[cols]();
			for (int j = 0; j < cols; ++j)
				m[i][j] = mat.m[i][j];
		}
		return *this;
	}
	else throw MatrixException("Type mismatch when attempting to call assignment operator on instance of Matrix class");
}

template <typename T>
bool Matrix<T>::operator==(const Matrix& opr) const
{
	if (cols == opr.cols && rows == opr.rows)
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				if (m[i][j] != opr.m[i][j])
					return false;
		return true;
	}
	else return false;
}

template <typename T>
Matrix<T> Matrix<T>::operator+(const Matrix& opr) const
{

	if (opr.cols == cols && opr.rows == rows)
	{
		Matrix<T> sum(rows, cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				sum.m[i][j] = m[i][j] + opr.m[i][j];
		return sum;
		/*
		if (this->isInt() && opr.isInt())
		{
			// Both operands are integer matricies; the sum matrix can also be an integer matrix
			Matrix<int> sum(rows, cols);
			for (int i = 0; i < rows; ++i)
				for (int j = 0; j < cols; ++j)
					sum.m[i][j] = m[i][j] + opr.m[i][j];
			return sum;
		}
		else
		{
			// At least one operand is a double matrix; the sum must be a double matrix
			Matrix<double> sum(rows, cols);
			for (int i = 0; i < rows; ++i)
				for (int j = 0; j < cols; ++j)
					sum.m[i][j] = m[i][j] + opr.m[i][j];
			return sum;
		}
		*/
	}
	else throw new MatrixException("Cannot add two matricies of differing sizes");
}

template <typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix& opr)
{
	if (rows == opr.rows && cols == opr.cols)
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				m[i][j] += opr.m[i][j];
		return *this;
	}
	else throw MatrixException("Cannot add two matricies of differing sizes");
}

template <typename T>
Matrix<T> Matrix<T>::operator-(const Matrix& opr) const
{
	if (rows == opr.rows && cols == opr.cols)
	{
		Matrix<T> diff(rows, cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				diff.m[i][j] = m[i][j] - opr.m[i][j];
		return diff;
	}
	else throw MatrixException("Cannot subtract two matricies of differing sizes");
}

template <typename T>
Matrix<T>& Matrix<T>::operator-=(const Matrix& opr)
{
	if (rows == opr.rows && cols == opr.cols)
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				m[i][j] -= opr.m[i][j];
		return *this;
	}
	else throw MatrixException("Cannot subtract two matricies of differing sizes");
}

template <typename T>
Matrix<T> Matrix<T>::operator*(int opr) const
{
	Matrix<T> mult(rows, cols);
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if constexpr (is_same_v<T, double>)
				mult.m[i][j] = m[i][j] * double(opr);
			else
				mult.m[i][j] = m[i][j] * opr;
	return mult;
}


template <typename T>
Matrix<T> Matrix<T>::operator*(double opr) const
{
	Matrix<double> mult(rows, cols);
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			mult.m[i][j] = double(m[i][j]) * opr;
	return mult;
}


template <typename T>
Matrix<T> Matrix<T>::operator*(const Matrix& opr) const
{
	if (cols == opr.rows)
	{
		Matrix<T> mult(rows, opr.cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < opr.cols; ++j)
			{
				int sum = 0;
				for (int k = 0; k < cols; ++k)
					sum += m[i][k] * opr.m[k][j];
				mult.m[i][j] = sum;
			}
		return mult;
	}
	else throw MatrixException("Matrix multiplication impossible: the number of columns in the first (lvalue) matrix must equal the number of rows of the second (rvalue) matrix");
}

template <typename T>
Matrix<T> Matrix<T>::operator~() const
{
	Matrix<T> trans(cols, rows);
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			trans.m[j][i] = m[i][j];
	return trans;
}

template <typename T>
Matrix<T> Matrix<T>::operator-() const
{
	if (rows == cols)
	{
		if constexpr (is_same_v<T, double>)
		{
			Matrix<T> inv(*this);
			Matrix<T> aug = inv | identity(rows);
			aug.rref();
			for (int i = 0; i < rows; ++i)
				for (int j = 0; j < cols; ++j)
					inv.setValue(i, j, aug.getValue(i, j + cols));
			return inv;
		}
		else throw MatrixException("Cannot invert an integer matrix");
	}
	else throw MatrixException("Cannot invert a non-square matrix");
}

template <typename T>
Matrix<T> Matrix<T>::operator|(const Matrix& opr) const
{
	if (rows == opr.rows)
	{
		int newcols = cols + opr.cols;
		Matrix<T> augment(rows, newcols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < newcols; ++j)
			{
				if (j < cols)
					augment.m[i][j] = m[i][j];
				else
					augment.m[i][j] = opr.m[i][j - cols];
			}
		return augment;
	}
	else throw MatrixException("Matricies must have the same number of rows to create augmented matrix");
}

template <typename T>
double Matrix<T>::det() const
{
	if (rows == cols)
	{
		if (rows == 1)
			return m[0][0];
		else
		{
			int d = 0;
			int s = 1;
			for (int j = 0; j < cols; ++j)
			{
				d += s * m[0][j] * this->minor(0, j).det();
				s = -1 * s;
			}
			return d;
		}
	}
	else throw new MatrixException("Cannot calculate determinant for a non-square matrix");
}

template <typename T>
Matrix<T> Matrix<T>::minor(int i, int j) const
{
	if (i >= 0 && i < rows && j >= 0 && j < cols)
	{
		if (rows == 1 || cols == 1)
			throw MatrixException("Cannot calculate a minor on a 1-dimensional matrix");
		else
		{
			Matrix<T> min(rows - 1, cols - 1);
			// 'r' and 'c' will be the indices used to traverse this matrix, while 'm' and 'n' will be used to populate the minor matrix
			int m = 0;
			int n = 0;
			for (int r = 0; r < rows; ++r)
			{
				if (r != i)
				{
					for (int c = 0; c < cols; ++c)
					{
						if (c != j)
						{
							min.m[m][n] = this->m[r][c];
							++n;
						}
					}
					++m;
					n = 0;
				}
			}
			return min;
		}
	}
	else throw MatrixException("Error calculating " + to_string(i) + "," + to_string(j) + " minor on matrix with " + to_string(rows) + " rows and " + to_string(cols) + " columns (out of bounds error)");
}

template <typename T>
void Matrix<T>::rref()
{
	if constexpr (is_same_v<T, int>)
		throw MatrixException("Cannot row-reduce an integer matrix directly--call get_rref() on this matrix instead which will return the rref form of this matrix as a Matrix<double> object");
	else
	{
		int lead = 0;
		for (int r = 0; r < rows; ++r)
		{
			if (lead > cols)
				return;
			int i = r;
			while (m[i][lead] == 0)
			{
				++i;
				if (i == rows)
				{
					i = r;
					++lead;
					if (lead == cols)
						return;
				}
			}
			rowSwap(i, r);
			rowMult(r, 1 / m[r][lead]);
			for (i = 0; i < rows; ++i)
				if (i != r)
					rowAdd(i, r, -1 * m[i][lead]);
		}
	}
}

template <typename T>
Matrix<double> Matrix<T>::get_rref() const
{
	Matrix<double> rr(this);
	/*
	if constexpr (is_same_v<T, int>)
		rr = this->doubleConvert();
	else
		rr = Matrix(this);
	*/
	rr.rref();
	return rr;
}

template <typename T>
Matrix<T> Matrix<T>::identity(int n) const
{
	Matrix<T> ident(n, n);
	for (int i = 0; i < n; ++i)
		ident.m[i][i] = 1;
	return ident;
}

template <typename T>
Matrix<double> Matrix<T>::doubleConvert() const
{
	if constexpr (is_same_v<T, int>)
	{
		Matrix<double> copy(rows, cols);
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
				copy.setValue(i, j, double(m[i][j]));
		return copy;
	}
	else throw MatrixException("doubleConvert() can only be used on integer-type matricies (use the copy constructor instead)");
}

#endif